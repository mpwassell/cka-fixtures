##
## Load fixtures from 'Output Website' sheet of scheduling spreadsheet.
##
import time
from django.core.management.base import BaseCommand, CommandError

from cka.models import Player,Club, Fixture, Period
from cka.season import season

def convert_code(tm):
    if tm == 'TIG3':
        return 'TIG1'
    elif tm == 'TIG4':
        return 'TIG2'
    elif tm == 'TIG5':
        return 'TIG3'
    elif tm == 'TIG6':
        return 'TIG4'
    else:
        return tm


class Command(BaseCommand):
    def handle(self, *args, **options):

        filename = r"E:\Mark\CKAWebSite\Fixtures2019\Fixtures.csv"

        Fixture.objects.filter(season__exact=season).delete()

        with open(filename,"r") as f:
            for line in f:
                if line[:8] == ",<ERROR>" or line[:8] == ",,,,,,,," or line[:9] == "Date,Time": continue
                data = line.split(",")

                data[4] = convert_code(data[4])
                data[5] = convert_code(data[5])
                code = data[4] + data[5] + "_" + str(season)
                date = data[1][:-1].strip() + " " + (data[2] if data[2] else "1:00 PM")
                date = time.strptime(date, '%d-%b-%Y %I:%M %p')

                #ds = time.strftime('%Y-%m-%d',date)
                #sp = Period.objects.filter(season__exact = season, date_from__lte = ds, date_to__gte = ds).get()
                sp=int(data[8])
                week=int(data[9])

                date = time.strftime('%Y-%m-%d %H:%M',date)

                print ("FIXTURE ", code, date)

                f = Fixture(code=code,season
                    =season,division=data[3],match=data[10].rstrip(),week=week,
                    date=date,home_team_id=data[4],away_team_id=data[5],by_hand=False,score_in_fixtures_live=False,
                    cancelled=False,ignore_for_status=False,home_score=None,away_score=None,notes=None,home_score_halftime=None,away_score_halftime=None,
                    referee_team_id=data[6],venue_id=data[7], changenotes=None,changestatus="",period=sp, playoff=False)
                f.save()

                print ("Check DST timezone handled correctly.")
